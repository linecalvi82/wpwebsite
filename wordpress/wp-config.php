<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wpoptique' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[m]o+,7#DG!yZ:m`D[ZxjZQ;?z-8#Y?1l28?OkUI{NpRcv2v*t~7?r}Y_tX+-2zF' );
define( 'SECURE_AUTH_KEY',  '.qr|%C7Y4SM`M;)h(L?7h&i_~ Eqn7,_tXyP_g3&K?8X&ygG5 O[Ktw-%4E[9:5_' );
define( 'LOGGED_IN_KEY',    'N_KQ@Oj|SD*lO=6Zs{gKLiuhU{J-EG!ijtH^wLjt]KU.-[0:.OlR9E90twH;8n,_' );
define( 'NONCE_KEY',        'INO@f4E/we.pDd6niukfn?TydY^!#NHx-(B0V1;K%_(?5KYFXRASfz~TE5P-#?^D' );
define( 'AUTH_SALT',        '**,0VMwP*=LJ]!%6f(iv>P8.AA<5k;NsJA(|p4B9ug-IRHLR@9VmUlTsn>i8yE#~' );
define( 'SECURE_AUTH_SALT', '^v3}*MB2jWMhttTR/i/Xy(:~6;fgPssG4:a?]En,-_=X6SNfig[)*(F+^>2a76Jn' );
define( 'LOGGED_IN_SALT',   'QzS 3m<}t4fnt,O-%GDB$wVEFj:& `T&~GcqBcs{= >,lbUgk;Ka8Pyv{251>(=H' );
define( 'NONCE_SALT',       '}<DpLT?M7v0esM6-iD%VDzs&yq; uPFGFt*%1}EHv19nl)@uw4+L`MO,)xae`X74' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
