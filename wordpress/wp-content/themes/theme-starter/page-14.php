<?php get_header(); ?>

<div class="single-info row mt-40" style="margin-top:2em;">
  <div class="col-lg-1 col-md-12 mt-120">
  </div>
  <div class="col-lg-4 col-md-12 mt-120 no-padding info-left">
    <h1>Contactez nous!</h1>
    <p><?php the_field("description"); ?></p>
    <?php echo do_shortcode( '[contact-form-7 id="17" title="Formulaire de contact 1"]' ); ?>
  </div>
  <div class="col-lg-6 col-md-12 no-padding info-right">
    <img src="<?php echo get_template_directory_uri(); ?>/img/boutiqueDurasOptiqueAudition.jpg" class="img-fluid" style="margin-top:2em;" alt="Magasin">
  </div>
</div>

<?php get_footer(); ?>
