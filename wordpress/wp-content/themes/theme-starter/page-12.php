<?php

get_header(); ?>

    <main role="main" id="main">

      <div class="single-info row mt-40" style="margin-top:0em;">
        <div class="col-lg-3 col-md-12 mt-120">
        </div>
        <div class="col-lg-6 col-md-12 mt-120 no-padding">
          <img src="<?php echo get_template_directory_uri(); ?>/img/salleDepistageAuditif.png" class="img-fluid" style="margin-top:2em;" alt="Salle dépistage auditif">
        </div>
        <div class="col-lg-2 col-md-12 mt-120">
        </div>
      </div>

      <div class="single-info row mt-40" style="margin-top:3em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>
        <div class="col-lg-10 col-md-12 mt-120 no-padding info-left">
          <h1><?php the_field("question1"); ?> </h1>
        </div>
      </div>

      <div class="single-info row mt-40" style="margin-top:2em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>
        <div class="col-lg-10 col-md-12 mt-120 no-padding info-left">
          <p><?php the_field("answer1"); ?>
          </p>
        </div>
      </div>

      <div class="single-info row mt-40" style="margin-top:2em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>
        <div class="col-lg-10 col-md-12 mt-120 no-padding info-left">
          <h2 style="font-size:40px;"><?php the_field("question2"); ?></h2>
        </div>
      </div>

      <div class="single-info row mt-40" style="margin-top:2em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>
        <div class="col-lg-10 col-md-12 mt-120 no-padding info-left">
          <p><?php the_field("answer2"); ?> </p>
        </div>
      </div>

      <div class="single-info row mt-40" style="margin-top:2em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>
        <div class="col-lg-10 col-md-12 mt-120 no-padding info-left">
          <h2 style="font-size:40px;"><?php the_field("question3"); ?></h2>
        </div>
      </div>

      <div class="single-info row mt-40" style="margin-top:2em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>
        <div class="col-lg-10 col-md-12 mt-120 no-padding info-left">
          <p><?php the_field("answer3"); ?></p>

        </div>
      </div>

      <div class="single-info row mt-40" style="margin-top:2em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>
        <div class="col-lg-10 col-md-12 mt-120 no-padding info-left">
          <h2 style="font-size:40px;"><?php the_field("question4"); ?></h2>
        </div>
      </div>

      <div class="single-info row mt-40" style="margin-top:2em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>
        <div class="col-lg-10 col-md-12 mt-120 no-padding info-left">
          <p><?php the_field("answer4"); ?></p>
        </div>
      </div>

    </main>

<?php get_footer(); ?>
