<?php get_header(); ?>

<div class="single-info row mt-40" style="margin-top:2em;">
  <div class="col-lg-12 col-md-12 mt-120 text-center no-padding info-left">
    <div class="info-thumb">
      <h1 style="font-size:50px;"> Duras Optique Audition
      </h1>
    </div>
  </div>
</div>

<div class="single-info row mt-40" style="margin-top:2em;">
  <div class="col-lg-1 col-md-12 mt-120">
  </div>
  <div class="col-lg-5 col-md-12 mt-120 text-center no-padding info-left">
    <div class="info-thumb">
      <img src="<?php echo get_template_directory_uri(); ?>/img/lunettes.jpg" class="img-fluid" style="margin-top:2em;" alt="Lunettes vitrine">
    </div>
  </div>
  <div class="col-lg-5 col-md-12 no-padding info-rigth">
    <div class="info-content">
      <h2 class="pb-30">Qui sommes nous?</h2>
      <p>
        <?php the_field("description"); ?>
      </p>

      </div>
  </div>
</div>

<div class="single-info row mt-40" style="margin-top:2em;">

  <div class="col-lg-1 col-md-12 mt-120">
  </div>

  <div class="col-lg-5 col-md-12 mt-120 text-center">
    <h2 style="font-size:40px;">Nos actualités</h2>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v6.0"></script>
    <div class="fb-page" data-href="https://www.facebook.com/durasopticiens/" data-tabs="timeline" data-width="500" data-height="" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/durasopticiens/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/durasopticiens/">Duras Optique Audition</a></blockquote></div>

  </div>

  <div class="col-lg-5 col-md-12 mt-120">
    <h2 class="text-center" style="font-size:40px;">Nos coordonnées</h2>
    <p></br> <img src="<?php echo get_template_directory_uri(); ?>/img/icon-adress.png" style="width:13%;" alt="icone adresse"> <?php the_field("address"); ?> </br>
      <img src="<?php echo get_template_directory_uri(); ?>/img/icon-horloge.png" style="width:13%;" alt="icone horloge"> <?php the_field("opening_time"); ?> </br>
      <img src="<?php echo get_template_directory_uri(); ?>/img/icon-rendezvous3.png" style="width:13%;" alt="icone voiture"> <?php the_field("travel"); ?> </br>
      <img src="<?php echo get_template_directory_uri(); ?>/img/icon-telephone.png" style="width:13%;" alt="icone téléphone"> <?php the_field("tel"); ?>
    </p>
  </div>
</div>


<div class="single-info row mt-40" style="margin-top:2em;">
  <div class="col-lg-12 col-md-12 mt-120 text-center no-padding info-left">
    <div class="info-thumb">
      <h2 style="font-size:40px;"> Où nous trouver?

      </h2>
    </div>
  </div>
</div>

<div class="single-info row mt-40" style="margin-top:2em;">

  <div class="col-lg-1 col-md-12 mt-120">
  </div>
  <div class="col-lg-10 col-md-12 mt-120 text-center">
    <div id="map" style="margin-top:2em;">
    <!-- Ici s'affichera la carte -->
    </div>
  </div>
</div>


<!-- Fichiers Javascript -->
        <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
	<script type="text/javascript">
  // Where you want to render the map.
  var element = document.getElementById('map');

  // Height has to be set. You can do this in CSS too.
  element.style = 'height:70%;';
  element.style = 'width:50%;'
  element.style = 'margin-top:3em;'

  // Create Leaflet map on map element.
  var map = L.map(element);

  // Add OSM tile leayer to the Leaflet map.
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  // Target's GPS coordinates.
  var target = L.latLng(<?php the_field("lat"); ?>, <?php the_field("long"); ?>);

  // Set map's center to target with zoom 14.
  map.setView(target, 14);

  // Place a marker on the same location.
  L.marker(target).addTo(map);
        </script>

<?php get_footer(); ?>
