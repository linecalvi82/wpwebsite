<!doctype html>
<html <?php language_attributes(); ?> <?php body_class(); ?> >
<head>
    <meta charset="<?php bloginfo('charset'); ?>">

    <title><?php wp_title(''); ?></title>

   <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <?php wp_head(); ?>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- LIEN CSS -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/styles.css">

     <link href="//www.google-analytics.com" rel="dns-prefetch">
</head>
<body class="no-js" style="background-color:#f8f9fa;">

<!-- wrapper -->
<div class="wrapper">

    <!-- header -->
    <header id="header" role="banner">
      <div class="single-info row mt-40">

        <div class="col-lg-1 col-md-12 mt-120">
        </div>

        <div class="col-lg-10 col-md-12 mt-120 text-center">

          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand text-left" href="http://localhost/wpsiteoptique/wordpress/" id="logo">Duras Optique Audition</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <?php  echo wp_nav_menu( array ('menu_class'=>'navbar-nav mr-auto test1')); ?>
              <!--<center>
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active text-right">
                  <a class="nav-link text-right" href="http://localhost/wpsiteoptique/wordpress/">Accueil <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active text-right">
                  <a class="nav-link text-right" href="http://localhost/wpsiteoptique/wordpress/produitsetservices/">Nos produits et services <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active text-right">
                  <a class="nav-link text-right" href="http://localhost/wpsiteoptique/wordpress/audio/">Audio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active text-right">
                  <a class="nav-link text-right" href="http://localhost/wpsiteoptique/wordpress/contact/">Contact <span class="sr-only">(current)</span></a>
                </li>
              </ul>
            </center>-->
            </div>
          </nav>
        </div>
      </div>
    </header>
    <!-- /header -->
