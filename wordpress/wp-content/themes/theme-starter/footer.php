			<!-- footer -->
			<footer class="footer" role="contentinfo">

				<div class="single-info row mt-40" id="marginFooter">
				  <div class="col-lg-1 col-md-12 mt-120">
				  </div>
				  <div class="col-lg-4 col-md-12 mt-120 no-padding info-left" id="madeBy">
				    &copy;<?php echo date(Y);?> Site réalisé par Line Calvi
				  </div>
				  <div class="col-lg-6 col-md-12 no-padding info-right text-right" id="menuFooter">
						<a href="http://localhost/wpsiteoptique/wordpress/">Accueil | <span class="sr-only">(current)</span></a>
						<a href="http://localhost/wpsiteoptique/wordpress/produitsetservices/">Nos produits et services | <span class="sr-only">(current)</span></a>
						<a href="http://localhost/wpsiteoptique/wordpress/audio/">Audio |<span class="sr-only">(current)</span></a>
						<a href="http://localhost/wpsiteoptique/wordpress/contact/">Contact | <span class="sr-only">(current)</span></a>
						<a href="http://localhost/wpsiteoptique/wordpress/?page_id=3&">Politique de confidentialité <span class="sr-only">(current)</span></a>
				  </div>
				</div>

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

	</body>
</html>
