<?php
/*
	Template Name: Template liste des projets
*/

get_header(); ?>
    
    <main role="main" id="main">
        
        <h1><?php the_title(); ?></h1>
        
        <!-- section -->
        <section class="">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                
                <?php the_content(); ?>
                
                <div>
                    <?php
                    // The Query
                    $args = array(
                        'posts_per_page'	=> '3',
                        'post_type'     	=> 'projet',
                    );
                    query_posts( $args );
                    
                    // The Loop
                    if (have_posts()):
                        while (have_posts()) : the_post();
                            ?>
                            
                            <h3><?php the_title() ?></h3>
                        
                        <?php endwhile;
                    endif;
                    
                    wp_reset_query();
                    ?>
                </div>
                
                </article>
                <!-- /article -->
            
            <?php endwhile; ?>
            
            <?php endif; ?>
        </section>
        <!-- /section -->
    
    </main>

<?php get_footer(); ?>