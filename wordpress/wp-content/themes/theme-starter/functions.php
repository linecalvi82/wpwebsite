<?php
/*------------------------------------*\
	Functions
\*------------------------------------*/
//Ajout de l'image à la une sur les pages
add_theme_support( 'post-thumbnails' );

//Ajout des extraits sur les pages
function wpc_excerpt_pages()
{
    add_meta_box( 'postexcerpt', 'Extrait', 'post_excerpt_meta_box', 'page', 'normal', 'core' );
}
add_action( 'admin_menu', 'wpc_excerpt_pages' );

//Définition de l'emplacement des menus
function html5_menu()
{
    register_nav_menus( array( // On utilise un tableau afin de pouvoir spécifier la position de plusieurs menus
                               'header-menu' => __( 'Navigation principal', 'html5-blank' ),
                               'header-menu' => __( 'Navigation principal', 'html5-blank' ),
                               'header-menu' => __( 'Navigation principal', 'html5-blank' ),
                               'header-menu' => __( 'Navigation principal', 'html5-blank' ),
                               'header-menu' => __( 'Navigation principal', 'html5-blank' ),
                               // Main Navigation
                        ) );
}
add_action( 'init', 'html5_menu' );

/*---------------------------------------------------------------------*\
	Déclaration d'un type de contenu personnalisé - Custom Post Types
\*---------------------------------------------------------------------*/
function custom_post_types()
{
    register_post_type(
        'projet',
        array(
            'label'           => 'Projets',
            'labels'          => array(
                'name'               => 'Projets',
                'singular_name'      => 'Projet',
                'all_items'          => 'Tous les projets',
                'add_new_item'       => 'Ajouter un projet',
                'edit_item'          => 'Éditer le projet',
                'new_item'           => 'Nouveau projet',
                'view_item'          => 'Voir le projet',
                'search_items'       => 'Rechercher parmi les projets',
                'not_found'          => 'Pas de projet trouvé',
                'not_found_in_trash' => 'Pas de projet dans la corbeille',
            ),
            'public'          => true,
            'capability_type' => 'post',
            'supports'        => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
            ),
            'has_archive'     => true,
        )
    );
}
add_action( 'init', 'custom_post_types' );

/*---------------------------------------------------------------------*\
	Déclaration d'une taxonomie personnalisée
\*---------------------------------------------------------------------*/
function custom_taxonomies()
{
    register_taxonomy(
        'type-projet', 'projet',
        array(
            'hierarchical' => true,
            'label'        => 'Type de projet',
            'show_ui'      => true,
            'query_var'    => 'type-projet',
        )
    );
}
add_action( 'init', 'custom_taxonomies' );

/*-------------------------------------------------------------------------------*\
	Déclaration de champs personnalisés (metabox) en utilisant le plugin metabox.io
\*-------------------------------------------------------------------------------*/
/*function rw_register_meta_boxes() {
    // Check if plugin is activated or included in theme
    if ( !class_exists( 'RW_Meta_Box' ) )
        return;

    $meta_boxes = array();

    // Ajout d'une box sur les types de contenu projet permettant d'inserer un lien externe.
    $meta_boxes[] = array(
        'id' => 'externalLink',
        'title' => 'Lien de votre projet',
        'pages' => array('projet'), // multiple post types and custom post type
        'context' => 'side',
        'priority' => 'default',
        'fields' => array(
            array(
                'name'  => '',
                'id'    => 'projet-external-link',
                'type'  => 'url'
            )
        )
    );

    foreach ( $meta_boxes as $meta_box )
    {
        new RW_Meta_Box( $meta_box );
    }
}*/

/**
 * Check if meta boxes is included
 *
 * @return bool
 */
/*function rw_maybe_include( $conditions ) {
    // Include in back-end only
    if ( ! defined( 'WP_ADMIN' ) || ! WP_ADMIN ) {
        return false;
    }

    // Always include for ajax
    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
        return true;
    }

    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    }
    elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }
    else {
        $post_id = false;
    }

    $post_id = (int) $post_id;
    $post    = get_post( $post_id );

    foreach ( $conditions as $cond => $v ) {
        // Catch non-arrays too
        if ( ! is_array( $v ) ) {
            $v = array( $v );
        }

        switch ( $cond ) {
            case 'id':
                if ( in_array( $post_id, $v ) ) {
                    return true;
                }
            break;
            case 'parent':
                $post_parent = $post->post_parent;
                if ( in_array( $post_parent, $v ) ) {
                    return true;
                }
            break;
            case 'slug':
                $post_slug = $post->post_name;
                if ( in_array( $post_slug, $v ) ) {
                    return true;
                }
            break;
            case 'category': //post must be saved or published first
                $categories = get_the_category( $post->ID );
                $catslugs = array();
                foreach ($categories as $category) {
                    array_push($catslugs, $category->slug);
                }
                if ( array_intersect( $catslugs, $v ) ) {
                    return true;
                }
            break;
            case 'template':
                $template = get_post_meta( $post_id, '_wp_page_template', true );
                if ( in_array( $template, $v ) ) {
                    return true;
                }
            break;
            case 'front-page':
                if ( $post_id == get_option( 'page_on_front' ) ) {
                    return true;
                }
            break;
        }
    }

    // If no condition matched
    return false;
}

add_action( 'admin_init', 'rw_register_meta_boxes' );*/