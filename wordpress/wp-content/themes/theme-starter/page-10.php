<?php
/*
/* PAGE ID -- : NOM DE LA PAGE
*/

get_header(); ?>

    <main role="main" id="main">
      <div class="single-info row mt-40" style="margin-top:2em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>
        <div class="col-lg-10 col-md-12 mt-120 no-padding">
          <h1>Nos produits et services :</h1>
        </div>
      </div>

      <div class="single-info row mt-40" style="margin-top:0em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>

        <div class="col-lg-3 col-md-12 mt-120 no-padding text-center">
          <img src="<?php echo get_template_directory_uri(); ?>/img/ProductsAndServicesPage/examenDeVue.png" class="img-fluid" style="margin-top:2em;width:30%;" alt="Examen de vue">
          <h2>Bilan visuel </h2>
          <p>Réalisation d'un bilan visuel gratuit, sur rendez-vous (examen non médical)</p>
        </div>

        <div class="col-lg-3 col-md-12 mt-120 no-padding text-center">
          <img src="<?php echo get_template_directory_uri(); ?>/img/ProductsAndServicesPage/appareilAuditif.png" class="img-fluid" style="margin-top:2em;width:30%;" alt="Appareil auditif">
          <h2>Dépistage auditif</h2>
          <p>Un test auditif gratuit réalisé par un audioprothésiste diplomé d'État </p>
        </div>

        <div class="col-lg-3 col-md-12 mt-120 no-padding text-center">
          <img src="<?php echo get_template_directory_uri(); ?>/img/ProductsAndServicesPage/lentilleDeContact.png" class="img-fluid" style="margin-top:2em;width:30%;" alt="Lentille de contact">
          <h2>Lentilles de contact</h2>
          <p>Possibilité de corriger votre vue à l'aide de lentilles de contact</p>
        </div>
      </div>

      <div class="single-info row mt-40" style="margin-top:2em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>
        <div class="col-lg-3 col-md-12 mt-120 no-padding text-center">
          <img src="<?php echo get_template_directory_uri(); ?>/img/ProductsAndServicesPage/carteVitale.png" class="img-fluid" style="margin-top:2em;width:30%;" alt="Carte vitale">
          <h2>Tiers payant</h2>
          <p>Ne vous occupez plus des papiers avec votre mutuelle, nous nous occupons de tout</p>
        </div>

        <div class="col-lg-3 col-md-12 mt-120 no-padding text-center">
          <img src="<?php echo get_template_directory_uri(); ?>/img/ProductsAndServicesPage/domicile.png" class="img-fluid" style="margin-top:2em;width:30%;" alt="Deplacement à domicile">
          <h2>Déplacement à domicile </h2>
          <p>Déplacement possible à domicile ou en EPADH</p>
        </div>

        <div class="col-lg-3 col-md-12 mt-120 no-padding text-center">
          <img src="<?php echo get_template_directory_uri(); ?>/img/ProductsAndServicesPage/paiement.png" class="img-fluid" style="margin-top:2em;width:30%;" alt="Carte bancaire">
          <h2>Paiement en plusieurs fois</h2>
          <p>Paiement en plusieurs fois disponible voir conditions en magasin</p>
        </div>
      </div>

      <div class="single-info row mt-40" style="margin-top:2em;">
        <div class="col-lg-1 col-md-12 mt-120">
        </div>

        <div class="col-lg-3 col-md-12 mt-120 no-padding text-center">
          <img src="<?php echo get_template_directory_uri(); ?>/img/ProductsAndServicesPage/sav.png" class="img-fluid" style="margin-top:2em;width:30%;" alt="Outils">
          <h2>Service après-vente</h2>
          <p>Entretien de vos lunettes et de vos appareils auditifs</p>
        </div>

        <div class="col-lg-3 col-md-12 mt-120 no-padding text-center">
          <img src="<?php echo get_template_directory_uri(); ?>/img/ProductsAndServicesPage/lunette.png" class="img-fluid" style="margin-top:2em;width:30%;" alt="Lunettes">
          <h2>Réajustage des montures</h2>
          <p>Si votre lunette glisse ou vous fait mal, venez la réajuster gratuitement en magasin</p>
        </div>

        <div class="col-lg-3 col-md-12 mt-120 no-padding text-center">
          <img src="<?php echo get_template_directory_uri(); ?>/img/ProductsAndServicesPage/livraison.png" class="img-fluid" style="margin-top:2em;width:30%;" alt="Camion">
          <h2>Possibilité de livraison</h2>
          <p>Livraison possible en france ou à l'internationale</p>
        </div>

      </div>


    </main>

<?php get_footer(); ?>
